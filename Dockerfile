FROM python:3

COPY eps.py requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt

USER 1000
CMD python3 eps.py
