import os
import sys
import time
import json
import logging
import paho.mqtt.client as paho

DEBUG = os.getenv("DEBUG", False)
if DEBUG == "True" or DEBUG == "1" or DEBUG == "on":
    DEBUG = True
else:
    DEBUG = False

EVCC_PHASES_TOPIC = os.getenv("EVCC_PHASES_TOPIC", "evcc-prod/loadpoints/2/phases")
WARP2_PHASES_TOPIC = os.getenv("WARP2_PHASES_TOPIC", "warp2/XSP/meter/state")


def on_connect(client, userdata, flags, rc):
    logging.info("Connecting to MQTT...")
    if rc == 0:
        mqtt.subscribe(WARP2_PHASES_TOPIC, 1)
        logging.info("Connection success")
    else:
        logging.error("Connection failure")


def on_disconnect(client, userdata, rc):
    connected = False


def on_message(mosq, userdata, msg):
    logging.debug("%s (qos=%s, r=%s) %s" % (msg.topic, str(msg.qos), msg.retain, str(msg.payload)))


def on_log(client, userdata, level, buf):
    logging.debug(buf)


def signal_handler(signal, frame):
    mqtt.loop_stop()
    logging.info("Shutting down MQTT client")
    mqtt.publish(EVCC_PHASES_TOPIC, payload="offline", qos=1, retain=True)
    mqtt.disconnect()
    sys.exit(0)


def handle_phases(client, userdata, msg):
    js = json.loads(msg.payload)
    if js["phases_connected"][0] and js["phases_connected"][1] and js["phases_connected"][2]:
        mqtt.publish(EVCC_PHASES_TOPIC, 3)
        logging.info("Setting EVCC to 3 phases")
    else:
        mqtt.publish(EVCC_PHASES_TOPIC, 1)
        logging.info("Setting EVCC to 1 phase")


mqtt = paho.Client("evcc-phase-switcher")
if DEBUG:
    logging.basicConfig(level=logging.DEBUG)
    logging.debug("Initializing...")
    mqtt.on_log = on_log
else:
    logging.basicConfig(level=logging.INFO)

mqtt.on_message = on_message
mqtt.on_connect = on_connect
mqtt.on_disconnect = on_disconnect
mqtt.message_callback_add(WARP2_PHASES_TOPIC, handle_phases)

mqtt.username_pw_set(os.getenv("MQTT_USER"), os.getenv("MQTT_PASSWORD"))
mqtt.connect("mqtt", 1883, 60)

mqtt.loop_start()

while True:
    time.sleep(10)
